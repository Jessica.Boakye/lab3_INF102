package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {
    

	@Override
    public int findNumber(RandomNumber number) {
        int lowerbound = number.getLowerbound();
        int upperbound = number.getUpperbound();
        return makeAnGuess(number, lowerbound, upperbound);
    }

    public int makeAnGuess(RandomNumber number, int lowerbound, int upperbound){
    //Vi må benytte binary search 
    /*
     * finn tallet i midten
     * 
     */

    int center = (upperbound + lowerbound) / 2;
    int gjetter = number.guess(center);

    if(gjetter == 0 || lowerbound == upperbound){
        return center;
    }
    if(gjetter == 1){
        return makeAnGuess(number, lowerbound, center-1);

    }
    if(gjetter == -1){
        return makeAnGuess(number, center+1, upperbound);
    }
    throw new IllegalArgumentException("Something went wrong");

}

}
