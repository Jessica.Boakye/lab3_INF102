package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        int n = numbers.size();
        //sjekker om listen inneholder kun et element, returner det elementet
        if(n == 1){
            return numbers.get(0);
        }
        //sammenlikner første element med andre element 
        if (numbers.get(0) >= numbers.get(1)){
            //første elememtet er større/lik andre element, første element returneres
            return numbers.get(0);
        } else{ 
            //hvis første elementet ikke er et peak element, så fjernes det første elementet
            numbers.remove(0);
            return peakElement(numbers);
        }
    }


        //bruker samme kode fra PeakIterative
        /* 
        int n = numbers.size();
        if(n == 0) {
            throw new IllegalArgumentException("Empty list");
        }

        if(n == 1){
            return numbers.get(0);
        }

        if(numbers.get(0) > numbers.get(1)){
            return numbers.get(0);
        }

        if(numbers.get(n-1) > numbers.get(n-2)){
            return numbers.get(n-1);
        }
        return 0;
          */ 
    }


