package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    private long firstNumber(List<Long> list){
        return list.remove(0);
    }

    @Override
    public long sum(List<Long> list) {
        //sjekker om listen er tom
        if(list.size()  == 0) {
            return 0;
        }
        //sjekker om listen inneholder kun et element
        if(list.size() == 1){
            return list.get(0);
        }
        //legger til det første elementet i listen til sum
        return firstNumber(list) + sum(list);
    }


    
}
